import great_expectations as ge
import pandas as pd 


class TableLevel:

    def __init__(self, df):
        self.df = ge.from_pandas(df)        
        self.essential_cols = [
            'id',
            'title',
            'description',
            'status',
            'date',
            'procurementMethod',
            'currency',
            'buyer',
            'uri/url',
            'sector',
            'subsector',
            'country_name',
            'country_code',
            'date',
            'map_coordinates',
            'indicator_name',
            'country_income_class',
            'state_name',
            'state_id',
            'subdivision_name',
            'units',
            'value',
            'region_name',
            'region_code',
            'geo_shape'
        ]
    
    def check_cols_exist(self):
        res_dict = dict()
        for _ in self.essential_cols:
            res = self.df.expect_column_to_exist(_)
            res_dict[_] = res['success']
        return res_dict
    
    def check_unique_cols(self):
        results = []
        for _ in ['id' , 'title' , 'description']:
            res = self.df.expect_column_values_to_be_unique(_)
            results.append({
                    'column': _,
                    'passedPercent': 100 - res['result']['unexpected_percent'],
                    'success': res['success']
                }
            )

        return results


class ColumnLevel(TableLevel):

    def __init__(self, df):
        super().__init__(df)
        self.col_existence_dict = super().check_cols_exist()
        self.code_lists = {
            'procurementMethod': ['open', 'selective', 'limited', 'direct', None],
            'status': ['planning', 'planned', 'active', 'cancelled', 'unsuccessful', 'complete', 'withdrawn', None],
        }


    def vals_in_set(self):
        result = []
        for _ in self.code_lists.keys():
            if self.col_existence_dict[_]:
                res = self.df.expect_column_values_to_be_in_set(_,self.code_lists[_])
                result.append({
                    'column': _,
                    'passedPercent': 100 - res['result']['unexpected_percent'],
                    'success': res['success'],
                    'codelist':self.code_lists[_]
                })
            else:
                result.append({
                    'column': _,
                    'passedPercent': 0,
                    'success': False
                })
        
        return result


    def check_timestamp(self, strftime_format):
        if self.col_existence_dict['date']:
            res = self.df.expect_column_values_to_match_strftime_format('date', strftime_format)

        res_dict = {
            'column': 'date',
            'passedPercent': 100 - res['result']['unexpected_percent'],
            'success': res['success'],
        }

        return [res_dict]
