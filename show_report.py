from validation_scipt import TableLevel, ColumnLevel
import streamlit as st
import pandas as pd
from taiyo_dp_connect.DataProduct import DataProduct
import logging as _logging


logging = _logging.getLogger('taiyo-sample-app')
logging.setLevel(_logging.DEBUG)
logging.addHandler(_logging.StreamHandler())
short_code = 'ads-b'
version = 'v1'
dp = DataProduct(short_code, version)
logging.info(f'Record Counts:{dp.count()}')

df = dp.load_df()

# df = pd.read_csv('./test_data/test.csv')
tl = TableLevel(df)
cl = ColumnLevel(df)
t0 = pd.DataFrame([tl.check_cols_exist()]).T
t1 = pd.DataFrame(tl.check_unique_cols())
t2 = pd.DataFrame(cl.vals_in_set())



st.title("Data report")

st.header("Table level report")

"Essential columns exist"

t0 = t0.rename(columns={0:'exists'},)
t0

"Column values are unique"
t1
"\n"
st.header("Column level results: ")
"Values belong to a codelists"
t2
